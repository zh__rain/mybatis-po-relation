package cn.com.agree.mybatisporelation.one2many.service.exception;

public class BizBookException extends RuntimeException {

    public BizBookException() {
    }

    public BizBookException(String message) {
        super(message);
    }

    public BizBookException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizBookException(Throwable cause) {
        super(cause);
    }

    public BizBookException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
