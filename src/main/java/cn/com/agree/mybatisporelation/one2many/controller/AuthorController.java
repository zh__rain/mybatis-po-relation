package cn.com.agree.mybatisporelation.one2many.controller;

import cn.com.agree.mybatisporelation.one2many.controller.dto.AuthorDTO;
import cn.com.agree.mybatisporelation.one2many.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/authors")
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @PutMapping
    public AuthorDTO addAuthor(@RequestBody AuthorDTO authorDTO) {
        return authorService.addAuthor(authorDTO);
    }

    @GetMapping("/{id}")
    public AuthorDTO getAuthor(@PathVariable("id") Long id) {
        return authorService.getAuthor(id);
    }

    @GetMapping("/another-way/{id}")
    public AuthorDTO getAuthorAnotherWay(@PathVariable("id") Long id) {
        return authorService.getAuthorAnotherWay(id);
    }

}
