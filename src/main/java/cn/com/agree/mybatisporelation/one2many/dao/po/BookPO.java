package cn.com.agree.mybatisporelation.one2many.dao.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@TableName("tb_book")
public class BookPO {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String title;

    @JsonIgnoreProperties(value = {"books"}, allowSetters = true)
    @TableField("author_id")
    private AuthorPO author;

}
