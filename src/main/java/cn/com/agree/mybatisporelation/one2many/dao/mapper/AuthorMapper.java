package cn.com.agree.mybatisporelation.one2many.dao.mapper;

import cn.com.agree.mybatisporelation.one2many.dao.po.AuthorPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AuthorMapper extends BaseMapper<AuthorPO> {

    AuthorPO findById(Long id);

    AuthorPO findByIdAnotherWay(Long id);

}
