package cn.com.agree.mybatisporelation.one2many.dao.mapper;

import cn.com.agree.mybatisporelation.one2many.dao.po.BookPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BookMapper extends BaseMapper<BookPO> {

    @Override
    int insert(BookPO entity);

    BookPO findById(Long id);

    BookPO findByIdAnotherWay(Long id);
}
