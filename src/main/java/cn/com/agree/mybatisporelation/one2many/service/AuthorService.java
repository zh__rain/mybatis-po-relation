package cn.com.agree.mybatisporelation.one2many.service;

import cn.com.agree.mybatisporelation.one2many.controller.dto.AuthorDTO;

public interface AuthorService {

    AuthorDTO addAuthor(AuthorDTO authorDTO);

    AuthorDTO getAuthor(Long id);

    AuthorDTO getAuthorAnotherWay(Long id);
}
