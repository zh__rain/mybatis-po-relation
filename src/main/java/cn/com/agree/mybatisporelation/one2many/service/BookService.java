package cn.com.agree.mybatisporelation.one2many.service;

import cn.com.agree.mybatisporelation.one2many.controller.dto.BookDTO;

public interface BookService {
    BookDTO addBook(BookDTO bookDTO);

    BookDTO getBook(Long id);

    BookDTO getBookAnotherWay(Long id);
}
