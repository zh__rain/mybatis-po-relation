package cn.com.agree.mybatisporelation.one2many.service.exception;

public class BizAuthorException extends RuntimeException {

    public BizAuthorException() {
    }

    public BizAuthorException(String message) {
        super(message);
    }

    public BizAuthorException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizAuthorException(Throwable cause) {
        super(cause);
    }

    public BizAuthorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
