package cn.com.agree.mybatisporelation.one2many.controller.dto;

import lombok.Data;

@Data
public class BookDTO {

    private Long id;

    private String title;

    private AuthorDTO author;

}
