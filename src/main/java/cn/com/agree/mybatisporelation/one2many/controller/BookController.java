package cn.com.agree.mybatisporelation.one2many.controller;

import cn.com.agree.mybatisporelation.one2many.controller.dto.BookDTO;
import cn.com.agree.mybatisporelation.one2many.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @PutMapping
    public BookDTO addBook(@RequestBody BookDTO bookDTO) {
        return bookService.addBook(bookDTO);
    }

    @GetMapping("/{id}")
    public BookDTO getBook(@PathVariable("id") Long id) {
        return bookService.getBook(id);
    }

    @GetMapping("/another-way/{id}")
    public BookDTO getBookAnotherWay(@PathVariable("id") Long id) {
        return bookService.getBookAnotherWay(id);
    }

}
