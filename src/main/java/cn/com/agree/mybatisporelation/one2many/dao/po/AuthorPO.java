package cn.com.agree.mybatisporelation.one2many.dao.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@TableName("tb_author")
public class AuthorPO {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    @JsonIgnoreProperties(value = {"author"}, allowSetters = true)
    @TableField(exist = false)
    private List<BookPO> books;

}
