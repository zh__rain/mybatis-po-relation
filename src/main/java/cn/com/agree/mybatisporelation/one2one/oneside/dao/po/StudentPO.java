package cn.com.agree.mybatisporelation.one2one.oneside.dao.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_student")
public class StudentPO {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    @TableField("address_id")
    private AddressPO address;

}
