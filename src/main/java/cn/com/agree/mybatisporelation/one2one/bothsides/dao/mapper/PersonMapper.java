package cn.com.agree.mybatisporelation.one2one.bothsides.dao.mapper;

import cn.com.agree.mybatisporelation.one2one.bothsides.dao.po.PersonPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PersonMapper extends BaseMapper<PersonPO> {


    PersonPO findById(Long id);

    PersonPO findByIdAnotherWay(Long id);
}
