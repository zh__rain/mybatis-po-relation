package cn.com.agree.mybatisporelation.one2one.bothsides.service;

import cn.com.agree.mybatisporelation.one2one.bothsides.controller.dto.PassportDTO;

public interface PassportService {

    PassportDTO addPassport(PassportDTO passportDTO);

    PassportDTO getPassport(Long id);
}
