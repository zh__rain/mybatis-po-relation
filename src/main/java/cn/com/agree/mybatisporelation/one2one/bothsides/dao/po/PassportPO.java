package cn.com.agree.mybatisporelation.one2one.bothsides.dao.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@TableName("tb_passport")
@Data
public class PassportPO {

    @TableId(type = IdType.AUTO)
    private Long id;

    @TableField("passport_number")
    private String passportNumber;

    @JsonIgnoreProperties(value = {"passport"}, allowSetters = true)
    @TableField("person_id")
    private PersonPO person;

}
