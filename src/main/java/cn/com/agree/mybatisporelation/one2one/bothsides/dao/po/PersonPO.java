package cn.com.agree.mybatisporelation.one2one.bothsides.dao.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@TableName("tb_person")
@Data
public class PersonPO {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    @TableField(exist = false)
    @JsonIgnoreProperties(value = {"person"}, allowSetters = true)
    private PassportPO passport;

}
