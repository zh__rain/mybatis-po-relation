package cn.com.agree.mybatisporelation.one2one.bothsides.service.exception;

public class BizPersonException extends RuntimeException {

    public BizPersonException() {
    }

    public BizPersonException(String message) {
        super(message);
    }

    public BizPersonException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizPersonException(Throwable cause) {
        super(cause);
    }

    public BizPersonException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
