package cn.com.agree.mybatisporelation.one2one.oneside.dao.mapper;

import cn.com.agree.mybatisporelation.one2one.oneside.dao.po.AddressPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AddressMapper extends BaseMapper<AddressPO> {


}
