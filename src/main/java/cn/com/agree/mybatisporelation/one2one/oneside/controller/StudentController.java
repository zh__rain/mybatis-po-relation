package cn.com.agree.mybatisporelation.one2one.oneside.controller;

import cn.com.agree.mybatisporelation.one2one.oneside.controller.dto.StudentDTO;
import cn.com.agree.mybatisporelation.one2one.oneside.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PutMapping
    public StudentDTO addStudent(@RequestBody StudentDTO studentDTO) {
        return studentService.addStudent(studentDTO);
    }

    @GetMapping("{id}")
    public StudentDTO getStudent(@PathVariable("id") Long id) {
        return studentService.getStudent(id);
    }

    @GetMapping("/another-way/{id}")
    public StudentDTO getStudentAnotherWay(@PathVariable("id") Long id) {
        return studentService.getStudentAnotherWay(id);
    }

}
