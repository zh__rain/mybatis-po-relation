package cn.com.agree.mybatisporelation.one2one.oneside.service;

import cn.com.agree.mybatisporelation.one2one.oneside.controller.dto.StudentDTO;

public interface StudentService {

    StudentDTO addStudent(StudentDTO studentDTO);

    StudentDTO getStudent(Long id);

    StudentDTO getStudentAnotherWay(Long id);
}
