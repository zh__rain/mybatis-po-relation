package cn.com.agree.mybatisporelation.one2one.bothsides.dao.mapper;

import cn.com.agree.mybatisporelation.one2one.bothsides.dao.po.PassportPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PassportMapper extends BaseMapper<PassportPO> {

    @Override
    int insert(PassportPO entity);

    PassportPO findById(Long id);

}
