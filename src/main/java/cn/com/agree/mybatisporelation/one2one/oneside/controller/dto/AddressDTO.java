package cn.com.agree.mybatisporelation.one2one.oneside.controller.dto;

import lombok.Data;

@Data
public class AddressDTO {

    private String province;
    private String city;
    private String county;

}
