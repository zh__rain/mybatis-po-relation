package cn.com.agree.mybatisporelation.one2one.bothsides.service;

import cn.com.agree.mybatisporelation.one2one.bothsides.controller.dto.PersonDTO;

public interface PersonService {

    PersonDTO addPerson(PersonDTO personDTO);

    PersonDTO getPerson(Long id);

    PersonDTO getPersonAnotherWay(Long id);
}
