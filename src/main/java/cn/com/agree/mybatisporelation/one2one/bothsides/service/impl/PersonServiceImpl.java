package cn.com.agree.mybatisporelation.one2one.bothsides.service.impl;

import cn.com.agree.mybatisporelation.one2one.bothsides.controller.dto.PersonDTO;
import cn.com.agree.mybatisporelation.one2one.bothsides.dao.mapper.PersonMapper;
import cn.com.agree.mybatisporelation.one2one.bothsides.dao.po.PersonPO;
import cn.com.agree.mybatisporelation.one2one.bothsides.service.PersonService;
import cn.com.agree.mybatisporelation.one2one.bothsides.service.exception.BizPersonException;
import cn.hutool.core.bean.copier.BeanCopier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonMapper personMapper;

    @Override
    public PersonDTO addPerson(PersonDTO personDTO) {
        PersonPO personPO = BeanCopier.create(personDTO, new PersonPO(), null).copy();
        personMapper.insert(personPO);
        return BeanCopier.create(personPO, new PersonDTO(), null).copy();
    }

    @Override
    public PersonDTO getPerson(Long id) {
        PersonPO personPO = personMapper.findById(id);
        if (Objects.isNull(personPO)) {
            throw new BizPersonException("旅人不存在。");
        }
        return BeanCopier.create(personPO, new PersonDTO(), null).copy();
    }

    @Override
    public PersonDTO getPersonAnotherWay(Long id) {
        PersonPO personPO = personMapper.findByIdAnotherWay(id);
        if (Objects.isNull(personPO)) {
            throw new BizPersonException("旅人不存在。");
        }
        return BeanCopier.create(personPO, new PersonDTO(), null).copy();
    }

}
