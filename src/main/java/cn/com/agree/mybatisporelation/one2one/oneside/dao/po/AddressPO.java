package cn.com.agree.mybatisporelation.one2one.oneside.dao.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_address")
public class AddressPO {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String province;
    private String city;
    private String county;

}
