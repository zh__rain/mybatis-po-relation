package cn.com.agree.mybatisporelation.one2one.bothsides.controller;

import cn.com.agree.mybatisporelation.one2one.bothsides.controller.dto.PersonDTO;
import cn.com.agree.mybatisporelation.one2one.bothsides.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/persons")
public class PersonController {

    @Autowired
    private PersonService personService;

    @PutMapping
    public PersonDTO addPerson(@RequestBody PersonDTO personDTO) {
        return personService.addPerson(personDTO);
    }

    @GetMapping("/{id}")
    public PersonDTO getPerson(@PathVariable("id") Long id) {
        return personService.getPerson(id);
    }

    @GetMapping("/another-way/{id}")
    public PersonDTO getPersonAnotherWay(@PathVariable("id") Long id) {
        return personService.getPersonAnotherWay(id);
    }
}
