package cn.com.agree.mybatisporelation.one2one.bothsides.service.exception;

public class BizPassportException extends RuntimeException {

    public BizPassportException() {
    }

    public BizPassportException(String message) {
        super(message);
    }

    public BizPassportException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizPassportException(Throwable cause) {
        super(cause);
    }

    public BizPassportException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
