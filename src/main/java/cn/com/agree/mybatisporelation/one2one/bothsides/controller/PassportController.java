package cn.com.agree.mybatisporelation.one2one.bothsides.controller;

import cn.com.agree.mybatisporelation.one2one.bothsides.controller.dto.PassportDTO;
import cn.com.agree.mybatisporelation.one2one.bothsides.service.PassportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/passports")
public class PassportController {

    @Autowired
    private PassportService passportService;

    @PutMapping
    public PassportDTO addPassport(@RequestBody PassportDTO passportDTO) {
        return passportService.addPassport(passportDTO);
    }

    @GetMapping("/{id}")
    public PassportDTO getPassport(@PathVariable("id") Long id) {
        return passportService.getPassport(id);
    }
}
