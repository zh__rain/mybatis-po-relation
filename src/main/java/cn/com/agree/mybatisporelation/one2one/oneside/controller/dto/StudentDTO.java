package cn.com.agree.mybatisporelation.one2one.oneside.controller.dto;

import lombok.Data;

@Data
public class StudentDTO {

    private Long id;
    private String name;
    private AddressDTO address;

}
