package cn.com.agree.mybatisporelation.one2one.oneside.dao.mapper;

import cn.com.agree.mybatisporelation.one2one.oneside.dao.po.StudentPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.io.Serializable;

@Mapper
public interface StudentMapper extends BaseMapper<StudentPO> {

    @Override
    int insert(StudentPO entity);

    @Override
    StudentPO selectById(Serializable id);

    StudentPO findById(Long id);
}
