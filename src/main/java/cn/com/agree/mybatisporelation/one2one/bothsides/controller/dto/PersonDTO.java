package cn.com.agree.mybatisporelation.one2one.bothsides.controller.dto;

import lombok.Data;

@Data
public class PersonDTO {

    private Long id;

    private String name;

    private PassportDTO passport;

}
