package cn.com.agree.mybatisporelation.one2one.bothsides.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
public class PassportDTO {

    private Long id;

    private String passportNumber;

    private PersonDTO person;

}
